import uuid
from werkzeug.security import generate_password_hash, check_password_hash
import adminuser
import json
def generateID():
    return str(uuid.uuid1())

def checkpwd(password):

    if not password:
        message = {'code': 603, 'message': 'Password Empty'}
        return json.dumps(message)
    else:
        hashed = {'code': 604, 'hashed_pwd': generate_password_hash(password)}
        return json.dumps(hashed)

def checkmail(mongo, email, type):

    if not email:
        message = {'code': 605, 'message': 'Email Field Empty'}
        return json.dumps(message)
    else:
        email1 = list(email)
        if '@' not in email1:
            message = {'code': 608,
                       'message': 'Incorrect Mail Format'
                       }
            return json.dumps(message)
        else:
            if type == "admin":
               return adminuser.checkMail(mongo, email)
            elif type == "service":
                return adminuser.checkServiceMail(mongo,email)
            else:
                # TODO: Put the service mail checking, when you finish it.
                return 'Invalid..'

def invalid():
    message = {'code': 617, 'message': 'Invalid Access'}
    return json.dumps(message)

