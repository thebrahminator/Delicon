from flask import Flask, request, session
from flask_pymongo import PyMongo
import adminuser
import json
import sidekicks
app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'delicon'
app.config['MONGO_URI'] = 'mongodb://thebrahminator:beyblade@ds143201.mlab.com:43201/delicon'
app.secret_key = 'ThisIsSparta'
mongo = PyMongo(app)
@app.route('/')
def hello_world():
    return 'Hello World!'


'''
Session Data Schema
userID
loggedIn
username
'''


'''
Admin Schema 
adminID -- Uniquely identifying the ID
username -- a unique name for each user
password -- lolz
email -- for the email
ifAdmin -- unique number for admin (0 means not an admin)
adminkey -- checking if the person entering is really an admin
'''
#ADMIN MODULE STARTS
@app.route('/addAdmin', methods=['POST'])
def addAdmin():
    # TODO: Finish the entire thing, and then add session validation √
    if session['ifAdmin']:
        admin = mongo.db.admins
        adminID = sidekicks.generateID()
        username = request.form.get('username', '')
        username_check = adminuser.checkUser(mongo, username, path="add")
        username_check_dict = json.loads(username_check)
        if username_check_dict['code'] == 601 or username_check_dict['code'] == 600:
            return username_check
        password = request.form.get('password', '')
        saltedpassword = " "
        password_check = sidekicks.checkpwd(password)
        print(password_check)
        password_check_dict = json.loads(password_check)
        if password_check_dict['code'] == 603:
            return password_check
        elif password_check_dict['code'] == 604:
            saltedpassword = password_check_dict['hashed_pwd']
        email = request.form.get('email', '')
        checked_email = sidekicks.checkmail(mongo, email, type="admin")
        checked_email_dict = json.loads(checked_email)
        if checked_email_dict['code'] == 606 or checked_email_dict['code'] == 608:
            return checked_email
        ifAdmin = 1
        adminkey = request.form.get('adminkey', '')
        adminkey_check = adminuser.checkAdminKey(mongo, adminkey)
        adminkey_check_dict = json.loads(adminkey_check)
        if adminkey_check_dict['code'] == 618:
            return adminkey_check
        admin.insert({'ID': adminID, 'username': username, 'password': saltedpassword, 'email': email, 'status': ifAdmin,
                  })
        return 'Successfully Added the Admin to the dashboard'
    else:
        sidekicks.invalid()


@app.route('/setAdminKey', methods=['POST'])
def setAdminKey():
    if session['ifAdmin'] == True:
        adminskey = request.form.get('adminskey', '')
        if not adminskey:
            message = {'code': 615, 'message': 'Empty Admin Key'}
            return json.dumps(message)
        else:
            adminkey = mongo.db.adminkey
            adminkey.drop()
            adminkey.insert({'key': adminskey})
            message = {'code': 616, 'message': 'Successfully Added Admin Key to Database'}
            return json.dumps(message)
    else:
        return sidekicks.invalid()

@app.route('/allAdmins', methods=['GET'])
def allAdmins():
    return adminuser.retallAdmins(mongo=mongo, ifAdmin=session['ifAdmin'])

@app.route('/deleteAdmin', methods=['POST'])
def deleteAdmin():
    unm = request.form.get('username', '')
    status = adminuser.deleteAdmin(mongo, unm, session['ifAdmin'])
    return status

@app.route('/AdminLogin', methods=['POST'])
def AdminLogin():
    username = request.form.get('username', '')
    password = request.form.get('password', '')

    checked_user = adminuser.checkUser(mongo,username,path="login")
    checked_user_dict = json.loads(checked_user)
    print(checked_user_dict)
    if checked_user_dict['code'] == 602 or checked_user_dict['code'] == 617:
        return checked_user
    else:
        checked_password = adminuser.checkPassword(mongo, password, username)
        if checked_user_dict['code'] == 610:
            checked_password_dict = json.loads(checked_password)
            return checked_password_dict
        else:
            session['userID'] = checked_user_dict['ID']
            session['loggedIn'] = True
            session['username'] = checked_user_dict['username']
            if 'status' in checked_user_dict.keys() and checked_user_dict['status'] == 1:
                session['ifAdmin'] = True
            return 'LoggedIn Successfully'


@app.route('/checkIfLoggedIn', methods=['GET'])
def checkifLoggedIn():

    if session['loggedIn'] == True:
        print(session['ifAdmin'])
        message = {'code': 613, 'message': session['username'] + ' is Logged In'}
        return json.dumps(message)
    else:
        message = {'code': 614, 'message': 'No user is currently logged in'}
        return json.dumps(message)


@app.route('/AdminLogout', methods=['GET'])
def adminLogout():
    session['loggedIn'] = False
    session['ifAdmin'] = False
    message = {'code': 612, 'message': 'Successfully Logged Out'}
    return json.dumps(message)

#SERVICE STARTS HERE.
'''
Service Schema
ServiceID -- Unique Identification
Username -- for enabling service login. (comes in addServiceDetails)
Password -- Protection. (comes in addServiceDetails)
Location -- The area where it's located (Full fledged address) (Has to be in subclass)
Phone -- Mobile Number, multiple numbers not allowed
ifRegistered -- once the user generates a username, it'll appear here. 
'''
@app.route('/addService', methods=['POST'])
def addService(): #For sending link to the newly added service
    if session['ifAdmin'] == True:
        serviceName = request.form.get('serviceName', '')
        serviceMail = request.form.get('serviceMail', '')
        serviceType = request.form.get('serviceType', '')
        return adminuser.addService(mongo, name=serviceName, email=serviceMail, servType=serviceType)
    else:
        return sidekicks.invalid()

#TODO: Finish setting up a link to be sent to the srvices for completing the registration process.
if __name__ == '__main__':
    app.run(debug=True, port=5001)

