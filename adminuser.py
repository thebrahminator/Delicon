import json
from werkzeug.security import check_password_hash
import sidekicks
def checkUser(mongo, username, path):
    admins = mongo.db.admins
    if not username:
        message = {'code': 600, 'message': 'No username entered'}
        return json.dumps(message)
    checked_user = admins.find_one({'username': username})
    if checked_user:
        message = {'code': 601, 'message': 'Username Already exists'}
        if path == "login":
            if checked_user['status'] != 1:
                return sidekicks.invalid()
            checked_user.pop('_id', None)
            checked_user['code'] = 611
            return json.dumps(checked_user)

    else:
        message = {'code': 602, 'message': 'Username doesnt exist'}
    return json.dumps(message)

def checkMail(mongo, email):
    admins = mongo.db.admins
    checked_mail = {}
    checked_mail = admins.find_one({'email': email})
    if checked_mail:
        message = {'code': 606, 'message': 'Email Already exists'}
    else:
        message = {'code': 607, 'message': 'No Conflict'}
    return json.dumps(message)

def checkPassword(mongo, password, username):
    admins = mongo.db.admins

    admin = admins.find_one({'username': username})
    result = check_password_hash(admin['password'], password)
    if result:
        print(admin)
        return admin
    else:
        message = {
            'code': 610,
            'message': 'Incorrect Password'
        }

    return json.dumps(message)

def checkAdminKey(mongo, key):
    adminkeys = mongo.db.adminkey
    adminkey = adminkeys.find_one({'key': key})
    print("Inside Admin Key")
    print(adminkey)
    if adminkey:
        message = {'code': 619, 'message': 'Correct Key'}
        return json.dumps(message)
    else:
        message = {'code': 618, 'message': 'Wrong Admin key'}
        return json.dumps(message)

def retallAdmins(mongo, ifAdmin):
    if ifAdmin:
        admins = mongo.db.admins
        alladminlist = []
        alladmins = admins.find()
        for admin in alladmins:
            admin.pop('_id')
            admin.pop('password')
            if admin['status'] == 1:
                alladminlist.append(admin)
        return json.dumps(alladminlist)
    else:
        return sidekicks.invalid()

def deleteAdmin(mongo, username, ifAdmin):
    if ifAdmin:
        admins = mongo.db.admins
        admin = admins.find_one({
            'username': username
        })
        admin['status'] = 0
        admins.save(admin)
        message = {'code': 620, 'message': 'Admin Deleted'}
        return json.dumps(message)
    else:
        sidekicks.invalid()

def addService(mongo, name, email, servType):
    services = mongo.db.services
    serviceID = sidekicks.generateID()
    blank = ''
    type = "service"
    checked_email = sidekicks.checkmail(mongo, email, type)
    checked_email_dict = json.loads(checked_email)
    if checked_email_dict['code'] == 606 or checked_email_dict['code'] == 605 or checked_email_dict['code'] == 608:
        return checked_email
    services.insert({'ID': serviceID, 'serviceName': name, 'serviceEmail': email, 'servicePhone': blank,
                     'serviceUsername': blank, 'servicePassword': blank, 'ifRegistered': 0, 'serviceLocation': blank,
                     'serviceType': servType})
    message = {
        'code': 622,
        'message': 'Link Sent to the Service'
    }
    return json.dumps(message)


def checkServiceMail(mongo, email):
    services = mongo.db.services
    checked_mail = {}
    checked_mail = services.find_one({'email': email})
    if checked_mail:
        message = {'code': 606, 'message': 'Email Already exists'}
    else:
        message = {'code': 607, 'message': 'No Conflict'}
    return json.dumps(message)

